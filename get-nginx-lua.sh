#!/bin/sh -ex
set       -ex

HERE=`cd "\`dirname \"$0\"\`";pwd`
NUMCORES=2
PREFIX="$HERE/out"
SRC="$HERE/src"

cd_back()
{
    cd - > /dev/null
}

cd_here()
{
    where="$HERE"/"$1"
    mkdir -p "$where"
    cd "$where"
}

build_autoconf_project()
{
    cd_here "$1"
        ./configure --prefix="$PREFIX"
        make -j $NUMCORES
        make install
    cd_back
}

git_repo()
{
    url="$2"
    folder="$(basename $2 .git)"

    cd_here src
        case $1 in
            get)
                rm -rf "$folder"
                git clone --depth 1 "$url"
                ;;
            clean)
                rm -rf "$folder"
                ;;
        esac
    cd_back
}

web_tar_gz()
{
    url="$2"
    folder="$(basename $2 .tar.gz)"

    cd_here src
        case $1 in
            get)
                rm -rf "$folder"
                curl "$url" | tar -C "$(pwd)" -xvz
                ;;
            clean)
                rm -rf "$folder"
                ;;
        esac
    cd_back
}

NGINX_VERSION=1.7.0
LUAJIT_VERSION=2.0.2
LUAJIT_API_VERSION=2.0
PCRE_VERSION=8.35
ZLIB_VERSION=1.2.8

packages() # <get|clean>
{
    web_tar_gz $1 "http://softlayer-dal.dl.sourceforge.net/project/pcre/pcre/$PCRE_VERSION/pcre-$PCRE_VERSION.tar.gz"
    web_tar_gz $1 "http://zlib.net/zlib-1.2.8.tar.gz"
    web_tar_gz $1 "http://luajit.org/download/LuaJIT-$LUAJIT_VERSION.tar.gz"
    git_repo   $1 "https://github.com/openresty/lua-nginx-module.git"
    git_repo   $1 "https://github.com/simpl/ngx_devel_kit.git"
    web_tar_gz $1 "http://nginx.org/download/nginx-$NGINX_VERSION.tar.gz"
}

clean()
{
    packages clean

    rm -rf "$HERE/src"
    rm -rf "$HERE/out"
}

build()
{
    packages get

    #build_autoconf_project src/pcre-$PCRE_VERSION

    #build__autoconf_project src/zlib-$ZLIB_VERSION

    cd_here src/LuaJIT-$LUAJIT_VERSION
        make -j $NUMCORES
        make PREFIX="$PREFIX" install
    cd_back

    cd_here src/nginx-$NGINX_VERSION
        export LUAJIT_LIB="$PREFIX"/lib
        export LUAJIT_INC="$PREFIX"/include/luajit-$LUAJIT_API_VERSION
        ./configure \
            --prefix="$PREFIX" \
            --with-pcre="$SRC"/pcre-$PCRE_VERSION \
            --with-zlib="$SRC"/zlib-$ZLIB_VERSION \
            --with-cc-opt="-Wno-deprecated-declarations" \
            --add-module="$SRC"/ngx_devel_kit \
            --add-module="$SRC"/lua-nginx-module
        make -j
        make install
    cd_back
}

#clean
#exit

build
